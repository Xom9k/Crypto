# -*- coding: utf-8 -*-

import numpy as np
import random

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    key = input("Введите используемый ключ: ").upper()
    
    encrypt = ""

    X = len(key) 
    if len(text) % X == 0:
        Y = int(len(text) / X)
    else:
         Y = int((len(text) / X)+1)
    
    Matrix = []
    Temp = []
    
    while len(text) % (X*Y) != 0:
        text = text + " "
    
    key_digit = []
    
    for t in range(0, len(text), (X*Y)):
        for i in key:
            for j in range(len(alph)):
                if i == alph[j]:
                    key_digit.append(j)
        
        count = 0
        
        for i in range(Y):
            for j in range(X):
                Temp.append(text[count])
                count += 1
            if i % 2 == 1:
                Temp.reverse()
            Matrix.append(Temp.copy())
            Temp.clear()
            
        for i in range(X):
            minIndex = key_digit.index(min(key_digit))
            key_digit[minIndex] = 99
            for j in range(Y): 
                encrypt += Matrix[j][minIndex]
                
        Matrix.clear()
        key_digit.clear()
        text = text[(X*Y):]
                       
        encrypt = encrypt.replace(" ", "")
        print("\nЗашифрованный текст: " + encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    key = input("Введите используемый ключ: ").upper()
    
    encrypt, decrypt = text, ""
    Temp = []   
      
    X = len(key) 
    if len(encrypt) % X ==0:
        Y = int(len(encrypt) / X)
    else:
          Y = int((len(encrypt) / X)+1)
          
    print(X*Y)    
      
      
    Matrix = (np.ndarray(shape=(Y,X), dtype=np.object)).tolist()
    Matrix_V2 = (np.ndarray(shape=(Y,X), dtype=np.object)).tolist()
    
    key_digit = []
                    
    for i in key:
        for j in range(len(alph)):
            if i == alph[j]:
                key_digit.append(j)
            
    test = 0
    G = len(encrypt)
    while G % X*Y != 0:
        G += 1
        test += 1
           
    for i in range(Y):
        for j in range(X):
            if test != 0:
                Matrix[i][j] = "!"
                test -= 1
                
    Matrix_3 = (np.rot90(np.rot90(Matrix))).tolist() 
    
    count = 0
    Flag = False
    for i in Matrix_3:
        if Flag == True:
            Matrix_3[count].reverse()
            Flag = False
        count += 1
        Flag = True

    count = 0
    
    for i in range(X):
        minIndex = key_digit.index(min(key_digit))
        key_digit[minIndex] = 99
        for j in range(Y):
            if  Matrix_3[j][minIndex] != "!":
                Matrix_V2[j][minIndex] = encrypt[count]
                count += 1
      
    
    for i in range(Y):
        for j in range(X):
            Temp.append(Matrix_V2[i][j])
        if i % 2 == 1:
            Temp.reverse()
        decrypt += ''.join(str(e) for e in Temp)
        Temp.clear()
          
    decrypt = decrypt.replace("None", "")
    print("Расшифрованный текст: " + decrypt)
    
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_4_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nВертикальная перестановка ")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

 
    # T = len(text)
    # while T % len(key) != 0:
    #     T = T + 1

    # X = int(text / len(key))
    # check = text - len(text)
    
    # if X % 2 == 0:
    #     checkSwap = 0
    # else:
    #     checkSwap = 1

    # Y = len(key)
    
    # Matrix = (np.ndarray(shape=(Y,X), dtype=np.object)).tolist()


    # for i in range(len(alph)):
    #     for j in range(len(key)):
    #         if alph[i] == key[j]:
    #             Temp.append(j)

    # test = 0
    # for i in range(Y):
    #     for j in range(X):
    #         if Temp[i] <= check -1 and j == X-1 and checkSwap == 0:
    #            Matrix[j][Temp[i]] = ""
    #            test +=1
    #         elif Temp[i] > Y - check -1 and j == X-1 and checkSwap == 1:
    #             Matrix[j][Temp[i]] = ""
    #             test +=1
    #         else:
    #             Matrix[j][Temp[i]] = text[i*X +j - test]
    
    # swap = 0
    # for i in range(X):
    #     if swap == 0:
    #         for j in range(Y):
    #             decrypt += Matrix[i][j]
    #         swap = 1
    #     else:
    #         for j in range(Y-1,-1, -1):
    #             decrypt += Matrix[i][j]
    #         swap = 0            
