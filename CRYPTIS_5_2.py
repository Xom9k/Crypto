# -*- coding: utf-8 -*-
import random
import struct
# from pygost.utils import xrange
from sys import version_info
import re
import codecs


class Gamma_28147():

    xrange = range if version_info[0] == 3 else range
    
    abc = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" 

    SBOX = (
            (12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1),
            (6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11, 13, 0, 15),
            (11, 3, 5, 8, 2, 15, 10, 13, 14, 1, 7, 4, 12, 9, 6, 0),
            (12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11),
            (7, 15, 5, 10, 8, 1, 6, 13, 0, 9, 3, 14, 11, 4, 2, 12),
            (5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1, 4, 3, 14, 0),
            (8, 14, 2, 5, 6, 9, 1, 12, 15, 4, 11, 0, 13, 10, 3, 7),
            (1, 7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11, 2),
        )

    SEQ_ENCRYPT = (
        0, 1, 2, 3, 4, 5, 6, 7,
        0, 1, 2, 3, 4, 5, 6, 7,
        0, 1, 2, 3, 4, 5, 6, 7,
        7, 6, 5, 4, 3, 2, 1, 0,
    )
    SEQ_DECRYPT = (
        0, 1, 2, 3, 4, 5, 6, 7,
        7, 6, 5, 4, 3, 2, 1, 0,
        7, 6, 5, 4, 3, 2, 1, 0,
        7, 6, 5, 4, 3, 2, 1, 0,
    )

    C1 = 0x01010104
    C2 = 0x01010101

    BLOCKSIZE = 8

    def Sblock(self, s, _in):
        return (
            (s[0][(_in >> 0) & 0x0F] << 0) +
            (s[1][(_in >> 4) & 0x0F] << 4) +
            (s[2][(_in >> 8) & 0x0F] << 8) +
            (s[3][(_in >> 12) & 0x0F] << 12) +
            (s[4][(_in >> 16) & 0x0F] << 16) +
            (s[5][(_in >> 20) & 0x0F] << 20) +
            (s[6][(_in >> 24) & 0x0F] << 24) +
            (s[7][(_in >> 28) & 0x0F] << 28)
        )

    def shift(self,x): # Функция сдвига на 11 битов
        return ((x << 11) & (2 ** 32 - 1)) | ((x >> (32 - 11)) & (2 ** 32 - 1))


    def byte2int(self,data): # перевод байт в целое число
        temp = 0
        k = 0
        for i in data:
            temp += i * 2**k
            k +=8
        return temp

    def bin2byte(self,data): # перевод двоичной в байты
        byte = b''
        for i in range(0,len(data), 8):
            temp = (int(data[i:i+8],2))
            temp = (struct.pack("B", temp))
            byte = temp + byte
        return byte

    def block2ns(self,data):
        data = bytearray(data)
        return (
            data[0] | data[1] << 8 | data[2] << 16 | data[3] << 24,
            data[4] | data[5] << 8 | data[6] << 16 | data[7] << 24,
        )

    def ns2block(self,N1,N2):
        n1, n2 = N1,N2
        return bytes(bytearray((
            (n2 >> 0) & 0xFF, (n2 >> 8) & 0xFF, (n2 >> 16) & 0xFF, (n2 >> 24) & 0xFF,
            (n1 >> 0) & 0xFF, (n1 >> 8) & 0xFF, (n1 >> 16) & 0xFF, (n1 >> 24) & 0xFF,
        )))

    def replaceSymb(self,s): # функция замены символов на буквенные сокращения
        s = s.replace(".","тчк").replace(",","зпт")
        return s

    def Encrypt(self,data, key, seq): # Функция простой замены (сообщение, ключ, последовательность для шифрования)
        N1 = ""
        N2 = ""
        arr = []
        for i in range(0, len(key), 4):
            string = key[i:i+4]
            arr.append(self.byte2int(string))

        for i in range(0, len(data), 8): # Проходим по всему тексту в блоках 64
            N1 = data[i:int(i+self.BLOCKSIZE/2)]
            N2 = data[int(i+self.BLOCKSIZE/2):i+self.BLOCKSIZE]
            N1 = self.byte2int(N1)
            N2 = self.byte2int(N2)
            for j in range(len(seq)): # Шифруем 32 раза            
                N1SUM = (arr[seq[j]] + N1 ) %  2**32
                N1SUM = self.Sblock(self.SBOX, N1SUM)
                N1SUM = self.shift(N1SUM)
                N1, N2 = N1SUM ^ N2, N1   

        return N1,N2



    def strxor(self,a, b): # Функция сложения строк по модулю 2
        mlen = min(len(a), len(b))
        a, b, xor = bytearray(a), bytearray(b), bytearray(mlen)
        for i in self.xrange(mlen):
           xor[i] = a[i] ^ b[i]
        return bytes(xor)


    def GAMMA(self,data, SYNC, key, seq): # Функция гаммирования (сообщение, синхропосылка, ключ, последовательность для шифрования)
        arr = []
        for i in range(0, len(key), 4):
            string = key[i:i+4]
            arr.append(self.byte2int(string))

        gamma = []

        N1 = 0
        N2 = 0

        N3,N4 = self.Encrypt(SYNC, key, seq)
        
        N3 = (N3 + self.C2) % (2 ** 32)
        N4 = (N4 + self.C1) % (2 ** 32 - 1)

        N1 = N3
        N2 = N4
        
        for i in range(0, len(data), 8):
            for j in range(len(seq)):           
                    N1SUM = (arr[seq[j]] + N1 ) %  2**32
                    N1SUM = self.Sblock(self.SBOX, N1SUM)
                    N1SUM = self.shift(N1SUM)
                    N1, N2 = N1SUM ^ N2, N1
        
            gamma.append((self.ns2block(N1,N2)))
            
            N3 = (N1 + self.C2) % (2 ** 32)
            N4 = (N2 + self.C1) % (2 ** 32 - 1)
        
            N1 = N3
            N2 = N4
            
        test = self.strxor(b"".join(gamma), data)  
        return test


    def byte2string(self,byte):
        temp = ""
        for i in byte:
            temp += chr(i)
        return temp

    def string2byte(self,byte):
        temp = b""
        for i in byte:
            temp += (struct.pack("B", ord(i)))
        return temp
    
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    gost = Gamma_28147()
    text = gost.replaceSymb(text) # преобразуем текст
    text = text.encode('utf8') # Переводим в байты
    
    # Генерация случайной синхропосылки
    sync = ""
    for i in range(64):
        sync += str(random.randint(0,1))
    print("Используемая Синхропосылка: " + sync)
    sync = gost.bin2byte(sync) # Переводим в байты
    
    
    # Генерация случайного ключа
    key = ""
    for i in range(256):
        key += str(random.randint(0,1))
    print("Используемый Ключ: " + key)
    key = gost.bin2byte(key) # Переводим в байты
    
    encr = gost.GAMMA(text, sync, key, gost.SEQ_ENCRYPT) # GAMMA
    temp = []
    for i in encr:
        temp.append(i)
        
    print("Зашифрованное сообщение: ",temp)
  
def decryption():
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ")
    text = re.findall('(\d+)',text)
    gost = Gamma_28147()
    
    sync =  input("Введите используемую синхропосылку: ")
    sync = gost.bin2byte(sync) 
    
    key =  input("Введите используемый ключ: ")
    key = gost.bin2byte(key) 
    
    def string2byte(byte):
        temp = b""
        for i in byte:
            temp += (struct.pack("B", int(i)))
        return temp
    
    ESCAPE_SEQUENCE_RE = re.compile(r'''
        ( \\U........      # 8-digit hex escapes
        | \\u....          # 4-digit hex escapes
        | \\x..            # 2-digit hex escapes
        | \\[0-7]{1,3}     # Octal escapes
        | \\N\{[^}]+\}     # Unicode characters by name
        | \\[\\'"abfnrtv]  # Single-character escapes
        )''', re.UNICODE | re.VERBOSE)
    
    def decode_escapes(s):
        def decode_match(match):
            return codecs.decode(match.group(0), 'unicode-escape')
    
        return ESCAPE_SEQUENCE_RE.sub(decode_match, s)
    
    text = string2byte(text)
    decr = gost.GAMMA(text, sync, key, gost.SEQ_ENCRYPT) 
    
    print("Расшифрованный текст: " + decr.decode("utf-8"))


def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTIS_5_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nГаммирование Гост 28147-89")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()