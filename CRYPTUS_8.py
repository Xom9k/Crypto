# -*- coding: utf-8 -*-

import random
from math import gcd as bltin_gcd
import re 

# P = 37
# x = 4
# g = 13  

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt = []
    
    P = int(input("Введите параметр P: "))
    x = int(input("Введите параметр X: "))
    g = int(input("Введите параметр G: "))
    
    Flag =  True
    
    if P < len(text):
        print("P должен быть больше сообщ")
        Flag = False
    
    for i in range (2, P):
        if P % i == 0:
            print("Число не простое")
            Flag = False

    if 1<g<P and 1<x<P:
        print()
    else:
        print("Нужны другие x и g")
        Flag = False
    
    if Flag == True:
        y = (g ** x) % P 
        
        Eler = P - 1
        count = int(input("Введите число рандомизаторов: "))
        c = count-1
        tess = []  
        
        for i in range(2, 10000):
            if count != 0 and bltin_gcd(Eler, i) == 1:
                tess.append(i)
                count += -1
                 
        for i in text:
            ran = tess[random.randint(0,c)]
            a = (g ** ran) % P
            b = ((y ** ran) * alph.find(i) ) % P
            encrypt.append((a,b))              
           
        print("Используемыe ключи: P = %d; X = %d; G = %d; Y = %d " % (P,x,g,y))
        print("Зашифрованный текст:")
        print(encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    P = int(input("Введите параметр P: "))
    x = int(input("Введите параметр X: "))
    
    encrypt, decrypt = text, ""
    
    encrypt = re.findall('(\d+)',encrypt)
    print(encrypt)
    print(len(encrypt))
    for i in range(0, len(encrypt), 2):
        a = int(encrypt[i])
        b = int(encrypt[i+1])
        M = (b * a**(P-1-x)) % P
        decrypt += alph[M]
        
    print("Расшифрованный текст: " + decrypt)


def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_8.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nEl Gamal")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()
        
        
        
