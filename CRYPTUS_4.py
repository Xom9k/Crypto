# -*- coding: utf-8 -*-
import numpy as np
import random

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
       
    encrypt = ""
    
    Temp = np.matrix('''   1 0 1 1 1 1 1 1 1 1;
                           0 1 1 1 0 1 0 0 1 1;
                           1 0 1 1 1 0 1 1 1 0;
                           1 1 1 0 1 1 1 0 1 1;
                           1 0 1 1 1 1 1 1 1 1;
                           1 1 0 1 1 0 0 1 1 0''')
    
    Y_len = Temp.shape[0]
    X_len = Temp.shape[1]
    
    while len(text) % (Y_len*X_len) != 0:
        text +=  alph[random.randint(0, 31)]
    
    Matrix_1 = Temp.tolist()
    Matrix_2 = (np.rot90(Temp.T)).tolist()
    Matrix_3 = (np.rot90(np.rot90(Temp))).tolist()
    Matrix_4 = []
    Matrix_enc = (np.ndarray(shape=(Y_len,X_len), dtype=np.object)).tolist()
    
    count = 0
    for i in range(Y_len):
        Temp2 = Matrix_1[count]
        Temp2 = Temp2[::-1]
        Matrix_4.append(Temp2)
        count += 1
        
    count = 0
    for t in range(0, len(text), (Y_len*X_len)):                       
    
        for i in range(Y_len):
            for j in range(X_len):
                if Matrix_1[i][j] == 0:
                    Matrix_enc[i][j] = text[count]
                    count += 1
                    
        for i in range(Y_len):
            for j in range(X_len):
                if Matrix_3[i][j] == 0:
                    Matrix_enc[i][j] = text[count]
                    count += 1
                    
        for i in range(Y_len):
            for j in range(X_len):
                if Matrix_2[i][j] == 0:
                    Matrix_enc[i][j] = text[count]
                    count += 1
                    
        for i in range(Y_len):
            for j in range(X_len):
                if Matrix_4[i][j] == 0:
                    Matrix_enc[i][j] = text[count]
                    count += 1
                
        for i in range(Y_len):
            for j in range(X_len):
                encrypt += Matrix_enc[i][j]
    
    print("Решетка: " )      
    print(Temp)                            
    print("\nЗашифрованный текст: " + encrypt)
  
def decryption():

    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    encrypt = text
    decrypt = ""
    
    Temp = np.matrix('''   1 0 1 1 1 1 1 1 1 1;
                           0 1 1 1 0 1 0 0 1 1;
                           1 0 1 1 1 0 1 1 1 0;
                           1 1 1 0 1 1 1 0 1 1;
                           1 0 1 1 1 1 1 1 1 1;
                           1 1 0 1 1 0 0 1 1 0''')
    
    Y_len = Temp.shape[0]
    X_len = Temp.shape[1]
        
    Matrix_1 = Temp.tolist()
    Matrix_2 = (np.rot90(Temp.T)).tolist()
    Matrix_3 = (np.rot90(np.rot90(Temp))).tolist()
    Matrix_4 = []
    Matrix_dec = (np.ndarray(shape=(Y_len,X_len), dtype=np.object)).tolist()
     
    count = 0
    for i in range(Y_len):
        Temp2 = Matrix_1[count]
        Temp2 = Temp2[::-1]
        Matrix_4.append(Temp2)
        count += 1
        
    count = 0            
    for t in range(0, len(encrypt), (Y_len*X_len)):
        
         for i in range(Y_len):
            for j in range(X_len):
                Matrix_dec[i][j] = encrypt[count]
                count += 1
        
         for i in range(Y_len):
            for j in range(X_len):
                if Matrix_1[i][j] == 0:
                    decrypt += Matrix_dec[i][j]
                    
         for i in range(Y_len):
            for j in range(X_len):
                if Matrix_3[i][j] == 0:
                    decrypt += Matrix_dec[i][j]
                    
         for i in range(Y_len):
            for j in range(X_len):
                if Matrix_2[i][j] == 0:
                    decrypt += Matrix_dec[i][j]
                    
         for i in range(Y_len):
            for j in range(X_len):
                if Matrix_4[i][j] == 0:
                    decrypt += Matrix_dec[i][j]

            
    print("Расшифрованный текст: " + decrypt)
    
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_4.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nРешетка Кардано")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()
