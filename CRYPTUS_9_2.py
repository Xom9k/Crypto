# -*- coding: utf-8 -*-


# НЕЛЬЗЯ ИЗ ЯИЧНИЦЫ СНОВА СДЕЛАТЬ ЯЙЦО ТЧК
    
# P = 53
# G = 4
# X = 10
# K = 7

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
 
    P = int(input("Введите параметр P: "))
    G = int(input("Введите параметр G: "))
    X = int(input("Введите параметр X: "))
    K = int(input("Введите параметр K: "))
    
    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
    
    Flag = True
    
    for i in range(2, P // 2+1):
        if (P % i == 0):
            print("Число не простое")
            Flag = False
    
    if Flag == True:
        Y = G ** X % P
        
        if P > K:
            temp = P
        else:
            temp = K
        
        for i in range (2, temp):
            if P % i == 0 and K % i == 0:
                    print("Числа имееют общ делители")
                    Flag = False
                    
        if Flag == True:
            a = G ** K % P
            
            for i in range(100000000000):
                if (( X * a + K * i ) % (P-1)) == x:
                    b=i
                    break
        
        print("Используемыe ключи: Y = %d; X = %d; P = %d; K = %d " % (Y,X,P,K))
        print('Получателю отправляется: S = (%d, %d)' % (a,b))
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
 
    P = int(input("Введите параметр P: "))
    Y = int(input("Введите параметр Y: "))
    G = int(input("Введите параметр G: "))
    a = int(input("Введите полученное A: "))
    b = int(input("Введите полученное B: "))

    A1 = Y**a*a**b % P
    A2 = G**x % P
    
    print('Проверка подписи: A1=%d и A2=%d' % (A1,A2))
                

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_9_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nЭЦП El Gamal")
      print("1. Вычислить")
      print("2. Проверить")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

