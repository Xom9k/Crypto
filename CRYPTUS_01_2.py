# -*- coding: utf-8 -*-
 
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt, decrypt = "", ""
    
    # Шифрование Шифром Атбаш
    for i in text:
        if i in alph:
            c = alph.find(i)
            key = ( 32 - c - 1 ) % 32
            encrypt += alph[key]
                   
    print("\nЗашифрованный текст: " + encrypt)
    
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt, decrypt = "", ""
    # Расшифрование Шифра Атбаш
    for i in encrypt:
        if i in alph:
            c = alph.find(i)
            key = ( 32 - c - 1 ) % 32
            decrypt += alph[key]
            
    print("Расшифрованный текст: " + decrypt)
          
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_01_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
        
if __name__ == '__main__':
    try:
      print("\nШифр Атбаш")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()  
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()