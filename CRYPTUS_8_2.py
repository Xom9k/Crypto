# -*- coding: utf-8 -*-
import re

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt = []
    
    P = int(input("Введите параметр P: "))
    Q = int(input("Введите параметр Q: "))
    E = int(input("Введите параметр E: "))
    
   
    encrypt, decrypt = [], ""
    
    # P, Q = 7,11
    
    if P > Q:
        temp = P
    else:
        temp = Q
        
    Flag = True
    for i in range (2, temp):
        if P % i == 0 or Q % i ==0:
            if i == P or i == Q:
                print("Числа простые")
            else:
                print("Числа не простые")
                Flag = False
                
    N = P*Q
    
    Eler = (P-1)*(Q-1)
    # E = 13
    
    if E > Eler:
        temp = E
    else:
        temp = Eler
    
    for i in range (2, temp):
        if E % i == 0 and Eler % i ==0:
                print("Числа имееют общ делители")
                Flag = False
    
    for i in range(10000):
        if i * E % Eler == 1:
            D = i
            break
    
    if Flag == True:
        for i in text:
            abc = alph.find(i)**E % N
            encrypt.append(abc)
                
        print("Используемыe ключи: E = %d; N = %d; D = %d " % (E,N,D))
        print("Зашифрованный текст:")
        print(encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    N = int(input("Введите параметр N: "))
    D = int(input("Введите параметр D: "))
    
    encrypt, decrypt = text, ""
    encrypt = re.findall('(\d+)',encrypt)
    
    for i in encrypt:
        abc = int(i)**D % N
        decrypt += alph[abc]

    print("Расшифрованный текст: " + decrypt)

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_8_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nRSA")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()