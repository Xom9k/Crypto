# -*- coding: utf-8 -*-
 
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    encrypt, decrypt = "", ""
    
    # Шифрование Шифром Вижинера с самоключом
    pass2 = input("Введите ключ: ").upper()
    password = pass2 + text
    j = 0
    for i in text:
        if i in alph:
            c = alph.find(i)
            shag = alph.find(password[j])
            key = ( c + shag ) % 32
            j += 1 
            encrypt += alph[key]
                   
    print("\nЗашифрованный текст: " + encrypt)
            
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    encrypt, decrypt = "", ""
    
    # Расшифрование Шифра Вижинера с самоключом
    password = input("Введите ключ: ").upper()

    j = -1
    key = 0
    for i in encrypt:
        if i in alph:
            c = alph.find(i)
            if j > -1:
                shag = key  
            else:
                shag = alph.find(password[0])
            key = ( c - shag ) % 32
            j = key  
            decrypt += alph[key]
            
    print("Расшифрованный текст: " + decrypt)
   
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_02_3.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
 
        
if __name__ == '__main__':
    try:
      print("\nШифр Вижинера с самоключом")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

