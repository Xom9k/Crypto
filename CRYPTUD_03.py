# -*- coding: utf-8 -*-
 
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЩЬЫЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    text = text.replace('Й', 'И').replace('Ъ', 'Ь')
    encrypt, decrypt, text_holder = "", "", ""
    
    # Шифрование Шифром Плейфера
    password = input("Введите ключ: ").upper()
    abclist = ""
    matlist_main = []
    matlist_temp = []
    for i in range(len(text)-1):
        if text[i] == text[i+1]:
            text_holder += text[i]
            text_holder += "Ф"
        else:
            text_holder += text[i]
    text_holder += text[len(text)-1]
    text = text_holder    
    
    y = ''
    for i in password:
        if i not in y:
            y += i
            
    password = y
    
    for i in password:
        abclist += i
    for i in alph:
        if i not in password:
            abclist += i
            
    temp = 0
    for i in range(5):
        for j in range(6):
            matlist_temp.append(abclist[temp])
            temp += 1
        matlist_main.append(matlist_temp.copy())
        matlist_temp.clear()
        
    if len(text) % 2 == 1:
        text = text + "Я"    
        
    for t in range(0, len(text), 2):
        for i in range(5):
            for j in range(6):
                if matlist_main[i][j] == text[t]:
                    holder1_i = i
                    holder1_j = j
                if matlist_main[i][j] == text[t+1]:
                   holder2_i = i
                   holder2_j = j
        if holder1_i == holder2_i:
            encrypt += matlist_main[holder1_i][(holder1_j+1) % 6]
            encrypt += matlist_main[holder2_i][(holder2_j+1) % 6]
        if holder1_j == holder2_j:
            encrypt += matlist_main[(holder1_i+1) % 5][holder1_j]
            encrypt += matlist_main[(holder2_i+1) % 5][holder2_j]
        if holder1_i != holder2_i and holder1_j != holder2_j:
            encrypt += matlist_main[holder1_i][holder2_j]
            encrypt += matlist_main[holder2_i][holder1_j]
                   
    print("\nЗашифрованный текст: " + encrypt)
    
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    encrypt, decrypt, text_holder = "", "", ""
    password = input("Введите ключ: ").upper()
    abclist = ""
    matlist_main = []
    matlist_temp = []
    for i in range(len(text)-1):
        if text[i] == text[i+1]:
            text_holder += text[i]
            text_holder += "Ф"
        else:
            text_holder += text[i]
    text_holder += text[len(text)-1]
    text = text_holder    
    
    y = ''
    for i in password:
        if i not in y:
            y += i
            
    password = y
    
    for i in password:
        abclist += i
    for i in alph:
        if i not in password:
            abclist += i
            
    temp = 0
    for i in range(5):
        for j in range(6):
            matlist_temp.append(abclist[temp])
            temp += 1
        matlist_main.append(matlist_temp.copy())
        matlist_temp.clear()
        
    if len(text) % 2 == 1:
        text = text + "Я"  
    # Расшифрование Шифра Плейфера
    for t in range(0, len(text), 2):
        for i in range(5):
            for j in range(6):
                if matlist_main[i][j] == text[t]:
                    holder1_i = i
                    holder1_j = j
                if matlist_main[i][j] == text[t+1]:
                   holder2_i = i
                   holder2_j = j
        if holder1_i == holder2_i:
            decrypt += matlist_main[holder1_i][(holder1_j-1) % 6]
            decrypt += matlist_main[holder2_i][(holder2_j-1) % 6]
        if holder1_j == holder2_j:
            decrypt += matlist_main[(holder1_i-1) % 5][holder1_j]
            decrypt += matlist_main[(holder2_i-1) % 5][holder2_j]
        if holder1_i != holder2_i and holder1_j != holder2_j:
            decrypt += matlist_main[holder1_i][holder2_j]
            decrypt += matlist_main[holder2_i][holder1_j]
            
    print("Расшифрованный текст: " + decrypt)
        
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUD_03.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
 
if __name__ == '__main__':
    try:
      print("\nШифр Плейфера")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()  
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

    
        
        
        
        
        
        