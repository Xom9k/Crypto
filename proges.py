# -*- coding: utf-8 -*-

def main():

    print("""Выберите шифр:
          \n Шифры однозначной замены \n 1. Атбаш \n 2. Цезарь \n 3. Полибий 
          \n Шифры многозначнойзначной замены\n 4. Тритемий \n 5. Белазо \n 6. Виженер \n 7. S-блок Магмы 
          \n Блочные шифры\n 8. Матричный шифр \n 9. Шифр Плэйфера
          \n Шифры перестановки \n 10. Вертикальная перестановка \n 11. Решетка Кардано
          \n Шифры гаммирования \n 13. Блокнот К.Шеннона \n 14. Гаммирование ГОСТ 28147-89/Магма
          \n Поточные шифры \n 15. A5-1 \n 16. A5-2 
          \n Комбинационные шифры \n 18. Магма \n 19. Aes \n 20. Кузнечик
          \n Асимметричные шифры \n 25. RSA \n 26. Elgamal \n 27. ECC
          \n Генерация цифровой подписи \n 21. RSA \n 22. Elgamal \n 23. ГОСТ Р 34.10-94 \n 24. ГОСТ Р 34.10-2012
          \n 28. Обмен ключами по алгоритму Diffie–Hellman
          """)
    
    temp = input("Укажите выбранные номер: ")

    if temp == "1":
        exec(open('CRYPTUS_01_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('CRYPTUS_01_1.py', encoding="utf8").read())
    elif temp == "3":
        exec(open('CRYPTUS_01_3.py', encoding="utf8").read())
    elif temp == "4":
        exec(open('CRYPTUS_02_1.py', encoding="utf8").read())
    elif temp == "5":
        exec(open('CRYPTUS_02_2.py', encoding="utf8").read())
    elif temp == "6":
        exec(open('CRYPTUS_02_3.py', encoding="utf8").read())
    elif temp == "7":
        exec(open('CRYPTIS_02_2.py', encoding="utf8").read())        
    elif temp == "8":
        exec(open('CRYPTUD_03.py', encoding="utf8").read())        
    elif temp == "9":
        exec(open('CRYPTUD_03_2.py', encoding="utf8").read()) 
    elif temp == "10":
        exec(open('CRYPTUS_4_2.py', encoding="utf8").read())        
    elif temp == "11":
        exec(open('CRYPTUS_4.py', encoding="utf8").read())        
    elif temp == "13":
        exec(open('CRYPTUS_5.py', encoding="utf8").read())        
    elif temp == "14":
        exec(open('CRYPTIS_5_2.py', encoding="utf8").read())       
    elif temp == "15":
        exec(open('CRYPTUS_6.py', encoding="utf8").read())        
    elif temp == "16":
        exec(open('CRYPTUS_6_2.py', encoding="utf8").read())        
    elif temp == "18":
        exec(open('CRYPTUS_7_2.py', encoding="utf8").read())        
    elif temp == "19":
        exec(open('CRYPTUS_7.py', encoding="utf8").read())        
    elif temp == "20":
        exec(open('CRYPTUS_7_3.py', encoding="utf8").read())        
    elif temp == "25":
        exec(open('CRYPTUS_8_2.py', encoding="utf8").read())        
    elif temp == "26":
        exec(open('CRYPTUS_8.py', encoding="utf8").read())        
    elif temp == "27":
        exec(open('CRYPTUS_8_3.py', encoding="utf8").read())        
    elif temp == "21":
        exec(open('CRYPTUS_9.py', encoding="utf8").read())        
    elif temp == "22":
        exec(open('CRYPTUS_9_2.py', encoding="utf8").read())        
    elif temp == "23":
        exec(open('CRYPTUS_10.py', encoding="utf8").read())     
    elif temp == "24":
        exec(open('CRYPTUS_10_2.py', encoding="utf8").read())
    elif temp == "28":
        exec(open('CRYPTUS_11.py', encoding="utf8").read())
   
if __name__ == '__main__':
    main()