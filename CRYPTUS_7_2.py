# -*- coding: utf-8 -*-
import random
import struct
import re
import codecs
import binascii
from struct import pack
from typing import List
from gostcrypto.utils import add_xor
from gostcrypto.utils import zero_fill
from gostcrypto.gostoid import ObjectIdentifier

_BLOCK_SIZE_KUZNECHIK: int = 16
_BLOCK_SIZE_MAGMA: int = 8
_KEY_SIZE: int = 32

_S_BOX_MAGMA: tuple = (
    (0x0c, 0x04, 0x06, 0x02, 0x0a, 0x05, 0x0b, 0x09,
     0x0e, 0x08, 0x0d, 0x07, 0x00, 0x03, 0x0f, 0x01,),
    (0x06, 0x08, 0x02, 0x03, 0x09, 0x0a, 0x05, 0x0c,
     0x01, 0x0e, 0x04, 0x07, 0x0b, 0x0d, 0x00, 0x0f,),
    (0x0b, 0x03, 0x05, 0x08, 0x02, 0x0f, 0x0a, 0x0d,
     0x0e, 0x01, 0x07, 0x04, 0x0c, 0x09, 0x06, 0x00,),
    (0x0c, 0x08, 0x02, 0x01, 0x0d, 0x04, 0x0f, 0x06,
     0x07, 0x00, 0x0a, 0x05, 0x03, 0x0e, 0x09, 0x0b,),
    (0x07, 0x0f, 0x05, 0x0a, 0x08, 0x01, 0x06, 0x0d,
     0x00, 0x09, 0x03, 0x0e, 0x0b, 0x04, 0x02, 0x0c,),
    (0x05, 0x0d, 0x0f, 0x06, 0x09, 0x02, 0x0c, 0x0a,
     0x0b, 0x07, 0x08, 0x01, 0x04, 0x03, 0x0e, 0x00,),
    (0x08, 0x0e, 0x02, 0x05, 0x06, 0x09, 0x01, 0x0c,
     0x0f, 0x04, 0x0b, 0x00, 0x0d, 0x0a, 0x03, 0x07,),
    (0x01, 0x07, 0x0e, 0x0d, 0x00, 0x05, 0x08, 0x03,
     0x04, 0x0f, 0x0a, 0x06, 0x09, 0x0c, 0x0b, 0x02,)
)

class GOST34122015Magma:
    def __init__(self, key: bytearray):
        self.oid = ObjectIdentifier('1.2.643.7.1.1.5.1')
        self._cipher_iter_key: List[bytearray] = []
        self._expand_iter_key(key)
        self._expand_iter_key(key)
        self._expand_iter_key(key)
        self._expand_iter_key_final(key)
        key = zero_fill(key)

    def __del__(self):
        self.clear()

    def _expand_iter_key(self, key: bytearray) -> None:
        iter_key = bytearray(b'')
        for j in range(8):
            iter_key = bytearray(4)
            for i in range(4):
                iter_key[i] = key[(j * 4) + i]
            self._cipher_iter_key.append(iter_key)
        iter_key = zero_fill(iter_key)
        key = zero_fill(key)

    def _expand_iter_key_final(self, key: bytearray) -> None:
        for j in range(8):
            iter_key = bytearray(4)
            for i in range(4):
                iter_key[i] = key[28 - (j * 4) + i]
            self._cipher_iter_key.append(iter_key)
        iter_key = zero_fill(iter_key)
        key = zero_fill(key)

    @staticmethod
    def _cipher_t(data: bytearray) -> bytearray:
        result = bytearray(4)
        data = bytearray(data)
        for i in range(4):
            first_part_byte = data[i] & 0x0f
            sec_part_byte = (data[i] & 0xf0) >> 4
            first_part_byte = _S_BOX_MAGMA[(3 - i) * 2][first_part_byte]
            sec_part_byte = _S_BOX_MAGMA[(3 - i) * 2 + 1][sec_part_byte]
            result[i] = (sec_part_byte << 4) | first_part_byte
        return result

    @staticmethod
    def _cipher_add_32(op_a: bytearray, op_b: bytearray) -> bytearray:
        op_a = bytearray(op_a)
        op_b = bytearray(op_b)
        internal = 0
        result = bytearray(4)
        for i in range(3, -1, -1):
            internal = op_a[i] + op_b[i] + (internal >> 8)
            result[i] = internal & 0xff
        return result

    @staticmethod
    def _cipher_g(cipher_k: bytearray, cipher_a: bytearray) -> bytearray:
        cipher_k = bytearray(cipher_k)
        cipher_a = bytearray(cipher_a)
        internal = bytearray(4)
        result = bytearray(4)
        result_32 = 0
        internal = GOST34122015Magma._cipher_add_32(cipher_k, cipher_a)
        internal = GOST34122015Magma._cipher_t(internal)
        result_32 = internal[0]
        result_32 = (result_32 << 8) + internal[1]
        result_32 = (result_32 << 8) + internal[2]
        result_32 = (result_32 << 8) + internal[3]
        result_32 = (result_32 << 11) | (result_32 >> 21)
        result_32 = result_32 & 0xffffffff
        result = bytearray(pack('>I', result_32))
        return result

    @staticmethod
    def _cipher_g_iter_result(a_0: bytearray, a_1: bytearray) -> bytearray:
        result = bytearray(_BLOCK_SIZE_MAGMA)
        result[0:4] = a_1
        result[4:_BLOCK_SIZE_MAGMA] = a_0
        return result

    @staticmethod
    def _cipher_g_iter(cipher_k: bytearray, cipher_a: bytearray) -> tuple:
        cipher_k = bytearray(cipher_k)
        cipher_a = bytearray(cipher_a)
        a_0 = bytearray(4)
        a_1 = bytearray(4)
        cipher_g = bytearray(4)
        a_1 = cipher_a[0:4]
        a_0 = cipher_a[4:_BLOCK_SIZE_MAGMA]
        cipher_g = GOST34122015Magma._cipher_g(cipher_k, a_0)
        cipher_g = add_xor(a_1, cipher_g)
        return a_0, a_1, cipher_g

    @staticmethod
    def _cipher_g_prev(cipher_k: bytearray, cipher_a: bytearray) -> bytearray:
        a_0, a_1, cipher_g = GOST34122015Magma._cipher_g_iter(cipher_k, cipher_a)
        a_1 = a_0
        a_0 = cipher_g
        return GOST34122015Magma._cipher_g_iter_result(a_0, a_1)

    @staticmethod
    def _cipher_g_fin(cipher_k: bytearray, cipher_a: bytearray) -> bytearray:
        a_0, a_1, cipher_g = GOST34122015Magma._cipher_g_iter(cipher_k, cipher_a)
        a_1 = cipher_g
        return GOST34122015Magma._cipher_g_iter_result(a_0, a_1)

    @property
    def block_size(self) -> int:
        return _BLOCK_SIZE_MAGMA

    @property
    def key_size(self) -> int:
        return _KEY_SIZE

    def decrypt(self, block: bytearray) -> bytearray:
        result = bytearray(_BLOCK_SIZE_MAGMA)
        result = GOST34122015Magma._cipher_g_prev(
            self._cipher_iter_key[31], block
        )
        for i in range(30, 0, -1):
            result = GOST34122015Magma._cipher_g_prev(
                self._cipher_iter_key[i], result
            )
        result = GOST34122015Magma._cipher_g_fin(
            self._cipher_iter_key[0], result
        )
        return result

    def encrypt(self, block: bytearray) -> bytearray:
        result = bytearray(_BLOCK_SIZE_MAGMA)
        result = GOST34122015Magma._cipher_g_prev(
            self._cipher_iter_key[0], block
        )
        for i in range(1, 31):
            result = GOST34122015Magma._cipher_g_prev(
                self._cipher_iter_key[i], result
            )
        result = GOST34122015Magma._cipher_g_fin(
            self._cipher_iter_key[31], result
        )
        return result

    def clear(self) -> None:
        for i in range(32):
            self._cipher_iter_key[i] = zero_fill(self._cipher_iter_key[i])
            





def encryption():
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    
    key = binascii.unhexlify('ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff')
    temp1 = []
    test_cipher = GOST34122015Magma(key)
    
    while len(text) % 4 != 0:
        text += "ъ"
        
    for i in range(0, len(text), 4):
        b = binascii.hexlify(bytes(str.encode(text)))
        test = binascii.unhexlify(b)
        c = test_cipher.encrypt(test)
        temp1.append(c.hex())
        text = text[4:]
        
    print("Используемый ключ: ", key.hex())  
    print("Зашифрованный текст: ")
    print(temp1)
    
def decryption():
    text = input("Введите исходный текст: ")
    text = re.findall('(\w+)',text)
    key = binascii.unhexlify('ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff')
    test_cipher = GOST34122015Magma(key)
    decrypt = ""
    for i in text:
        temp2 = test_cipher.decrypt(binascii.unhexlify(i))
        d = binascii.hexlify(bytearray(temp2))
        d = binascii.unhexlify(d).decode('utf-8')
        decrypt += d
        
    decrypt = decrypt.replace("ъ", '')
    print("Используемый ключ: ", key.hex())  
    print("Расшифрование: ",decrypt)

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")
    
    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_7_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\n МАГМА ECB")
      print("1. Зашифровать")
      print("2. Расшифровать")
      print("3. Проврека по госту")
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
      elif temp == "3":
          key = binascii.unhexlify('ffeeddccbbaa99887766554433221100f0f1f2f3f4f5f6f7f8f9fafbfcfdfeff')
          text = binascii.unhexlify('fedcba9876543210')
          test_cipher = GOST34122015Magma(key)
          temp = test_cipher.encrypt(text)
          temp2 = test_cipher.decrypt(temp)
          print("Проверка контрольными примерами:")
          print("Шифрование: fedcba9876543210")
          print("Используемый ключ: ", key.hex()) 
          print("Результат // Эталон")
          print(temp.hex(),'4ee901e5c2d8ca3d')
          print("Расшифрование: 4ee901e5c2d8ca3d")
          print("Результат  // Эталон")
          print(temp2.hex(),'fedcba9876543210')
          menu()

    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()
        
        