# -*- coding: utf-8 -*-

# НЕЛЬЗЯ ИЗ ЯИЧНИЦЫ СНОВА СДЕЛАТЬ ЯЙЦО ТЧК

# p = 113
# q = 7
# X = 6
# k = 4

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
 
    p = int(input("Введите параметр P: "))
    q = int(input("Введите параметр Q: "))
    X = int(input("Введите параметр X: "))
    k = int(input("Введите параметр K: "))
    
    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
    
    
    Flag = True
    
    if p < q or X > q:
        Flag = False
    
    for i in range (2, p):
        if p % i == 0 and q % i == 0:
                print("Числа имееют общ делители")
                Flag = False
                
    for i in range(2, p-1):
           if (i**q) % p == 1:
               a=i
               break
           
    y = a**X % p    
    r = a**k % p % q
    
    if r == 0:
        print("Выберите другое k")
        Flag = False            
    
    if Flag == True:
     
        s = (X * r + k*x) % q
        
        print("Используемыe ключи: P = %d; Q = %d; A = %d; X = %d; Y = %d; " % (p,q,a,X,y))
        print('Получателю отправляется: (r,s) = (%d, %d)' % (r,s))
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
 
    p = int(input("Введите параметр P: "))
    q = int(input("Введите параметр Q: "))
    a = int(input("Введите параметр A: "))
    y = int(input("Введите параметр Y: "))
    s = int(input("Введите полученное S: "))
    r = int(input("Введите полученное R: "))

    v = x**(q-2) % q
    z1 = (s*v) % q
    z2 = ((q-r)*v) % q
    u =((a**z1*y**z2) % p) % q
    
    print('Проверка подписи: U=%d и R=%d' % (u,r))
                

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_10.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nЭЦП ГОСТ Р 34.10-94")
      print("1. Вычислить")
      print("2. Проверить")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

