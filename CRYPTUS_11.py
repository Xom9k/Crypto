# -*- coding: utf-8 -*-

# n = 34
# a = 15
# k1 = 14
# k2 = 22
    
def encryption():    
    n = int(input("Введите параметр n: "))
    a = int(input("Введите параметр  a: "))
    k1 = int(input("Введите секретный ключ 1: "))
    k2 = int(input("Введите секретный ключ 2: "))
    
    Flag = True

    if 1<a<n:
        print("Параметры верны")
    else:
        Flag = False
    
    if 2<k1<n-1 and 2<k2<n-1:
        print("Параметры верны")
    else:
        Flag = False
        
    if Flag == True:
        y1 = a ** k1 % n
        y2 = a ** k2 % n
        
        K1 = y2 ** k1 % n
        K2 = y1 ** k2 % n
        
        print("Используемыe ключи: N = %d; A = %d; K1 = %d; K2 = %d " % (n,a,k1,k2))
        print('Проверка общего ключа: K1 = %d, K2 = %d' % (K1,K2))

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_11.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nDiffe hellman")
      print("1. Обмен")

      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()

    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

