# -*- coding: utf-8 -*-

# БЕГА 
# a=2
# b=7
# p=11
# G = 7,10
# C = 6
# Y = calc(C,G[0],G[1])
# q = 7
# k = 5
# P = calc(k,G[0],G[1])

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
 
    a = int(input("Введите параметр кривой а: "))
    b = int(input("Введите параметр кривой b: "))
    p = int(input("Введите параметр кривой p: "))
    Gx = int(input("Введите параметр x точки G: "))
    Gy = int(input("Введите параметр y точки G: "))
    C = int(input("Введите параметр C: "))
    q = int(input("Введите параметр Q: "))
    k = int(input("Введите параметр K: "))
    
    G = Gx, Gy
    
    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) + 1
        x = new_hash(x,index)
    
    def calc(C, x, y):
        C =  bin(C)[3:]
        x0 = x
        y0 = y
        for i in range(len(C)):
            if C[i] == "0":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                x = xx
                y = yy
            if C[i] == "1":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                alpha = ((y0-yy)*(x0-xx)**(p-2)) % p
                xx2 =  ( (alpha**2)-xx-x0 ) % p
                yy2 = ((alpha*(xx-xx2)-yy) )% p
        
                x = xx2
                y = yy2
     
        return x, y

    Y = calc(C,G[0],G[1])
    P = calc(k,G[0],G[1])
    
    r = P[0] % q
    s = ( x*k + r*C ) % q

    print("Используемыe ключи: q = %d; G = (%d,%d); x = %d; k = %d " % (q,G[0],G[1],C,k))
    print('Получателю отправляется: (r,s) = (%d, %d)' % (r,s))
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    a = int(input("Введите параметр кривой а: "))
    b = int(input("Введите параметр кривой b: "))
    p = int(input("Введите параметр кривой p: "))
    Gx = int(input("Введите параметр x точки G: "))
    Gy = int(input("Введите параметр y точки G: "))
    C = int(input("Введите параметр C: "))
    q = int(input("Введите параметр Q: "))
    r = int(input("Введите полученное R: "))
    s = int(input("Введите полученное S: "))
    
    G = Gx, Gy
    
    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) + 1
        x = new_hash(x,index)
    
    def calc(C, x, y):
        C =  bin(C)[3:]
        x0 = x
        y0 = y
        for i in range(len(C)):
            if C[i] == "0":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                x = xx
                y = yy
            if C[i] == "1":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                alpha = ((y0-yy)*(x0-xx)**(p-2)) % p
                xx2 =  ( (alpha**2)-xx-x0 ) % p
                yy2 = ((alpha*(xx-xx2)-yy) )% p
        
                x = xx2
                y = yy2
     
        return x, y

    Y = calc(C,G[0],G[1])

       
    Flag = True
    
    if r == 0 or s ==0:
        print(r,s, "| R и S не должны быть равны 0")
        Flag = False
       
    if Flag == True:
        u1 = s*x**(q-2) % q
        u2 = -r*x**(q-2) % q
        
        U1 = calc(u1, G[0], G[1])
        U2 = calc(u2, Y[0], Y[1])
        
        if U1[0]==U2[0] and U1[1]==U2[1]:
            alpha = ((3*U1[0]**2+a)*(2*U1[1])**(p-2)) % p
            xx =  ( (alpha**2)-(2*U1[0]) ) % p
            yy = ((alpha*(U1[0]-xx)-U1[1]) )% p
        else:
            alpha = ((U1[1]-U2[1])*(U1[0]-U2[0])**(p-2)) % p
            xx =  ( (alpha**2)-U2[0]-U1[0] ) % p
            yy = ((alpha*(U2[0]-xx)-U2[1]) )% p
            print(alpha,xx,yy)
            
        if 0<r<q and 0<s<q:
            print("Парамерты прошли проверку")
        print('Проверка подписи: U=%d и R=%d' % (xx,r))

                

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_10_1.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nЭЦП ГОСТ Р 34.10-2012")
      print("1. Вычислить")
      print("2. Проверить")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

