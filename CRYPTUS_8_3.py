# -*- coding: utf-8 -*-
import random
import re
# a=2
# b=-5
# p=11
# C = 6
# Y = 5,8
# m = 10
# k = 5 

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    print(len(alph))
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    
    a = int(input("Введите параметр кривой а: "))
    b = int(input("Введите параметр кривой b: "))
    p = 37
    C = int(input("Введите параметр C: "))
    x = int(input("Введите параметр X(0): "))
    y = int(input("Введите параметр Y(0): "))
      
    Y = x,y
    
    def calc(C, x, y):
        C =  bin(C)[3:]
        x0 = x
        y0 = y
        for i in range(len(C)):
            if C[i] == "0":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                x = xx
                y = yy
            if C[i] == "1":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                alpha = ((y0-yy)*(x0-xx)**(p-2)) % p
                xx2 =  ( (alpha**2)-xx-x0 ) % p
                yy2 = ((alpha*(xx-xx2)-yy) )% p
        
                x = xx2
                y = yy2
        return x, y
   
    encrypt = []
    for i in text:
        k = random.randrange(1,6)
        t = alph.find(i) + 1
        
        G = calc(C,Y[0],Y[1])
        R = calc(k,Y[0],Y[1])
        P = calc(k,G[0],G[1])

        E = t * P[0] % p 

        encrypt.append((R[0],R[1],E))
        
    print("Зашифрованный текст: ", str(encrypt))
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    # text = input("Введите исходный текст: ").upper()
    # text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    text = (input("Введите зашифрованный текст: "))
    
    text = re.findall('(\d+)',text)
    
    a = int(input("Введите параметр кривой а: "))
    b = int(input("Введите параметр кривой b: "))
    p = 37
    C = int(input("Введите параметр C: "))

    def calc(C, x, y):
        C =  bin(C)[3:]
        x0 = x
        y0 = y
        for i in range(len(C)):
            if C[i] == "0":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                x = xx
                y = yy
            if C[i] == "1":
                alpha = ((3*x**2+a)*(2*y)**(p-2)) % p
                xx =  ( (alpha**2)-(2*x) ) % p
                yy = ((alpha*(x-xx)-y) )% p
        
                alpha = ((y0-yy)*(x0-xx)**(p-2)) % p
                xx2 =  ( (alpha**2)-xx-x0 ) % p
                yy2 = ((alpha*(xx-xx2)-yy) )% p
        
                x = xx2
                y = yy2
        return x, y

    decrypt = ""
    for i in range(0, len(text), 3):
    
        R = int(text[0]), int(text[1])
        E = int(text[2])
        
        Q = calc(C,R[0],R[1])
        m2 = E * Q[0]**(p-2) % p
        
        decrypt += alph[m2-1]
        text = text[3:]
                
    print("Расшифрованный текст: " + decrypt)

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_8_3.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nECC")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()