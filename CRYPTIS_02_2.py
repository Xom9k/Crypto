# -*- coding: utf-8 -*-

import numpy as np
 
def encryption():
    # Используемый алфавит
    
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
    
    encrypt, decrypt = "", ""
    
    Temp = np.matrix('''   12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1;
                           6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11,13, 0, 15;
                           11,3, 5, 8, 2, 15, 10, 13, 14, 1,7, 4, 12, 9, 6, 0;
                           12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11;
                           7, 15, 5, 10, 8, 1,6, 13, 0, 9, 3, 14, 11,4, 2, 12;
                           5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1,4, 3, 14, 0;
                           8, 14, 2, 5,6, 9, 1, 12, 15, 4, 11,0, 13, 10, 3, 7;
                           1,7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11,2''')
        
    Matrix = Temp.tolist()
    
    U, holder = "", ""
    
    if len(text) % 2 == 1:
        text = text + "Я"  
    
    for i in text:
        i = str(bin(ord(i))[2:])
        while len(i) != 16:
            i = "0" + i
        U += i
    
    check = 0
    for t in range(0, len(U), 4):
        test = int(U[t]+U[t+1]+U[t+2]+U[t+3], 2)
        temp = str(bin(Matrix[check][test])[2:])
        while len(temp) % 4 != 0:
            temp = "0" + temp
        holder += temp
        if len(holder) == 32:
            # holder = holder[-21:] + holder[:11]
            encrypt += chr(int(holder[:16], 2))
            encrypt += chr(int(holder[-16:], 2))
            holder = ""
        check = (check + 1 ) % 8
        
    print("Зашифрованный текст: " + encrypt)
            
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    encrypt, decrypt = text, ""
    
    Temp = np.matrix('''   12, 4, 6, 2, 10, 5, 11, 9, 14, 8, 13, 7, 0, 3, 15, 1;
                           6, 8, 2, 3, 9, 10, 5, 12, 1, 14, 4, 7, 11,13, 0, 15;
                           11,3, 5, 8, 2, 15, 10, 13, 14, 1,7, 4, 12, 9, 6, 0;
                           12, 8, 2, 1, 13, 4, 15, 6, 7, 0, 10, 5, 3, 14, 9, 11;
                           7, 15, 5, 10, 8, 1,6, 13, 0, 9, 3, 14, 11,4, 2, 12;
                           5, 13, 15, 6, 9, 2, 12, 10, 11, 7, 8, 1,4, 3, 14, 0;
                           8, 14, 2, 5,6, 9, 1, 12, 15, 4, 11,0, 13, 10, 3, 7;
                           1,7, 14, 13, 0, 5, 8, 3, 4, 15, 10, 6, 9, 12, 11,2''')
        
    Matrix = Temp.tolist()
    
    U, T, holder = "", "", ""
    
    for i in encrypt:
        i = str(bin(ord(i))[2:])
        while len(i) != 16:
            i = "0" + i
        U += i        
    
    # j = 0
    # for t in range(0, len(U), 32):
    #     test = U[0+32*j:32+32*j]
    #     test = test[-11:] + test[:21]
    #     T += test
    #     j += 1
    
    check = 0
    for t in range(0, len(U), 4):
        test = int(U[t]+U[t+1]+U[t+2]+U[t+3], 2)
        temp = str(bin(Matrix[check].index(test))[2:])
        while len(temp) % 4 != 0:
            temp = "0" + temp
        holder += temp
        if len(holder) == 32:
            decrypt += chr(int(holder[:16], 2))
            decrypt += chr(int(holder[-16:], 2))
            holder = ""
        check = (check + 1 ) % 8
        
    print("Расшифрованный текст: " + decrypt)
   
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTIS_02_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
 
        
if __name__ == '__main__':
    try:
      print("\nS-блок Гост-28147")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

