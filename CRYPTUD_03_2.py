# -*- coding: utf-8 -*-

import numpy as np
import re
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    encrypt, decrypt,  = [], []
    print("1 4 8; 3 7 2; 6 9 5")
    holder = input("Введите квадратную матрицу по примеру сверху: ")
    matlist_main = np.matrix(holder)
    
    try:
        matlist_inver = np.linalg.inv(matlist_main)
    except:
        print("Данная матрица не подходит")
        
    
    text_digit = []
    
    for i in text:
        for j in range(len(alph)):
            if i == alph[j]:
                text_digit.append(j+1)
                
    size = matlist_main.shape[0]
    temp = np.empty(0)
    count = 0
    
    while len(text_digit) % size !=0:
        text_digit.append(24)
    
    for t in range(0, len(text_digit), size):
        for i in range(size):
            temp = np.append(temp, text_digit[count])
            count += 1
        encrypt.append(matlist_main.dot(temp))
        temp = np.empty(0)
       
    text_holder = []
    
    for i in range(len(encrypt)):
        test = np.array(encrypt[i])
        for j in range(test.shape[1]):
            text_holder.append(int(test[0][j]))
            
    print("Матрица: " )      
    print(matlist_main)  
    print("Зашифрованный текст: " + str(text_holder))
                       
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()

    decrypt = []
    print("1 4 8; 3 7 2; 6 9 5")
    holder = input("Введите квадратную матрицу по примеру сверху: ")
    matlist_main = np.matrix(holder)
    size = matlist_main.shape[0]
    
    #Расшифровка
    matlist_inver = np.linalg.inv(matlist_main)
    temp = np.empty(0)
    count = 0
    
    text_holder = re.findall('(\d+)',text)
    
    print(text_holder)
    
    for t in range(0, len(text_holder), size):
        for i in range(size):
            temp = np.append(temp, int(text_holder[count]))
            count += 1
        decrypt.append(matlist_inver.dot(temp))
        temp = np.empty(0)
        
    text_holder = []

    for i in range(len(decrypt)):
        test = np.array(decrypt[i])
        for j in range(test.shape[1]):
            text_holder.append(int(round(test[0][j])))
            
    decrypt = ""
    for i in text_holder:
        for j in range(len(alph)):
            if i == j:
                decrypt+=alph[j-1]
                
    print("Расшифрованный текст: " + decrypt)
        
def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUD_03_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
 
if __name__ == '__main__':
    try:
      print("\nМатричный Шифр ")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()  
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

