# -*- coding: utf-8 -*-

import random
import re

master_key =  ""

for i in range(64):
    master_key += str(random.randint(0, 1))

def crypt(kadr):
        
    X1, X2, X3 = [0] * 19, [0] * 22, [0] * 23
    
    
    for i in range(len(master_key)):
        X1.insert(0,X1.pop())
        X2.insert(0,X2.pop())
        X3.insert(0,X3.pop())
        X1[0] = X1[0] ^ int(master_key[i]) ^ X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
        X2[0] = X2[0] ^ int(master_key[i]) ^ X2[20] ^ X2[21]
        X3[0] = X3[0] ^ int(master_key[i]) ^ X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
        
    kadr = str(bin(kadr)[2:])
    while len(kadr) != 22:
        kadr = "0" + kadr
        
    for i in range(len(kadr)):
        X1.insert(0,X1.pop())
        X2.insert(0,X2.pop())
        X3.insert(0,X3.pop())
        X1[0] = X1[0] ^ int(kadr[i]) ^ X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
        X2[0] = X2[0] ^ int(kadr[i]) ^ X2[20] ^ X2[21]
        X3[0] = X3[0] ^ int(kadr[i]) ^ X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
        
    def major(x,y,z):
         if(x + y + z > 1):
             return 1
         else:
             return 0
         
    for i in range(100):
        dimon = major(X1[8],X2[10],X3[10])
        
        if X1[8] == dimon:
            temp = X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
            X1.insert(0,X1.pop())
            X1[0] = temp
        if X2[10] == dimon:
            temp = X2[20] ^ X2[21]
            X2.insert(0,X2.pop())
            X2[0] = temp
        if X3[10] == dimon:
            temp = X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
            X3.insert(0,X3.pop())
            X3[0] = temp
    
    
    bit = []
    for i in range(114):
        dimon = major(X1[8],X2[10],X3[10])
        
        if X1[8] == dimon:
            temp = X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
            X1.insert(0,X1.pop())
            X1[0] = temp
        if X2[10] == dimon:
            temp = X2[20] ^ X2[21]
            X2.insert(0,X2.pop())
            X2[0] = temp
        if X3[10] == dimon:
            temp = X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
            X3.insert(0,X3.pop())
            X3[0] = temp
            
        bit.append(X1[18] ^ X2[21] ^ X3[22])
    return bit
 
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt, decrypt, text_bin, master_key =  [], "", [], ""
    
    kadr = int(input("Введите номер кадра: "))
    
    
    for i in text:
        i = str(bin(ord(i))[2:])
        text_bin.append(i)    
        print(i)
    
    count = 0  
    bits = []  
    temp = ""
    
    while len(bits) < len(text_bin)*11:
            bits += crypt(kadr)
            kadr += 1
            
    for j in range(len(text_bin)):
        test = text_bin[j]
        for t in range(len(test)):
            temp += str(int(bits[count]) ^ int(test[t]))
            print(temp)
            count += 1
        temp = int(temp, 2)
        encrypt.append(temp)
        temp = ""
        
    print("\nЗашифрованный текст: ")
    print(encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ")

    # Используемый ключ
    kadr = int(input("Введите номер кадра: "))
    
    encrypt, decrypt = text, ""
    encrypt = re.findall('(\d+)',encrypt)
    text_bin = []
    for i in encrypt:
        i = str(bin(int(i))[2:])
        while len(i) != 11:
            i = "0" + i
        text_bin.append(i)  
            
    count = 0  
    bits = []  
    temp = ""
    
    while len(bits) < len(text_bin)*11:
            bits += crypt(kadr)
            kadr += 1
            
    for j in range(len(text_bin)):
        test = text_bin[j]
        for t in range(len(test)):
            temp += str(int(bits[count]) ^ int(test[t]))
            count += 1
        temp = int(temp, 2)
        decrypt += chr(temp)
        temp = ""

    print("Расшифрованный текст: " + decrypt)

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_6.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nА5-1")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()
