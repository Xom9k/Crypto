# -*- coding: utf-8 -*-

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    a = int(input("Введите параметр a: "))
    c = int(input("Введите параметр c: "))
    x = int(input("Введите параметр x: "))
    
    encrypt, decrypt, text_digit, gamma = "", "", [], []
      
    m = 33
 
    while a % 4 != 1:
        a += 1
        
    for i in range(0,len(text)):
        x = (a * x + c) % m
        gamma.append(x)

    for i in range(len(text)):
        temp = ( (alph.index(text[i]) + gamma[i] ) % m )
        encrypt += alph[temp]
        
    print("\nЗашифрованный текст: " + encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЁЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    # Используемый ключ
    a = int(input("Введите параметр a: "))
    c = int(input("Введите параметр c: "))
    x = int(input("Введите параметр x: "))
    
    m = 33
    encrypt, decrypt, gamma= text, "", []
 
    for i in range(0,len(text)):
        x = (a * x + c) % m
        gamma.append(x)
    
    for i in range(len(text)):
        temp = ( (alph.index(text[i]) - gamma[i] ) % m )
        decrypt += alph[temp]
    
    print("Расшифрованный текст: " + decrypt)


def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_5.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nОдноразовый блокнот К. Шеннона")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()
