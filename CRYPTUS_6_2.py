# -*- coding: utf-8 -*-
import numpy as np
import re
import random

master_key =  ""

for i in range(64):
    master_key += str(random.randint(0, 1))

def crypt(kadr):
        
    X1, X2, X3, X4 = [0] * 19, [0] * 22, [0] * 23, [0] * 17
    
    
    for i in range(len(master_key)):
        X1.insert(0,X1.pop())
        X2.insert(0,X2.pop())
        X3.insert(0,X3.pop())
        X4.insert(0,X4.pop())
        X1[0] = X1[0] ^ int(master_key[i]) ^ X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
        X2[0] = X2[0] ^ int(master_key[i]) ^ X2[20] ^ X2[21]
        X3[0] = X3[0] ^ int(master_key[i]) ^ X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
        X4[0] = X4[0] ^ int(master_key[i]) ^ X4[16] ^ X2[11]
        
    kadr = str(bin(kadr)[2:])
    while len(kadr) != 22:
        kadr = "0" + kadr
        
    for i in range(len(kadr)):
        X1.insert(0,X1.pop())
        X2.insert(0,X2.pop())
        X3.insert(0,X3.pop())
        X4.insert(0,X4.pop())
        X1[0] = X1[0] ^ int(kadr[i]) ^ X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
        X2[0] = X2[0] ^ int(kadr[i]) ^ X2[20] ^ X2[21]
        X3[0] = X3[0] ^ int(kadr[i]) ^ X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
        X4[0] = X4[0] ^ int(kadr[i]) ^ X4[16] ^ X2[11]
        
    def major(x,y,z):
         if(x + y + z > 1):
             return 1
         else:
             return 0
         
    for i in range(100):
        dimon = major(X4[3],X4[7],X4[10])
        
        if X4[10] == dimon:
            temp = X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
            X1.insert(0,X1.pop())
            X1[0] = temp
        if X4[7] == dimon:
            temp = X2[20] ^ X2[21]
            X2.insert(0,X2.pop())
            X2[0] = temp
        if X4[3] == dimon:
            temp = X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
            X3.insert(0,X3.pop())
            X3[0] = temp
        temp = X4[16] ^ X4[11]
        X4.insert(0,X4.pop())
        X4[0] = temp
            
    
    bit = []
    for i in range(114):
        dimon = major(X4[3],X4[7],X4[10])
        
        if X4[10] == dimon:
            temp = X1[13] ^ X1[16] ^ X1[17] ^ X1[18]
            X1.insert(0,X1.pop())
            X1[0] = temp
        if X4[7] == dimon:
            temp = X2[20] ^ X2[21]
            X2.insert(0,X2.pop())
            X2[0] = temp
        if X4[3] == dimon:
            temp = X3[7] ^ X3[20] ^ X3[21] ^ X3[22]
            X3.insert(0,X3.pop())
            X3[0] = temp
        temp = X4[16] ^ X4[11]
        X4.insert(0,X4.pop())
        X4[0] = temp
            
        X1_holder = major(X1[12],X1[14],X1[15]) ^ X1[18]
        X2_holder = major(X2[9],X2[13],X2[16]) ^ X2[21]
        X3_holder = major(X3[13],X3[16],X3[18]) ^ X3[22] 
            
        bit.append(X1_holder ^ X2_holder ^ X3_holder)
    return bit
  
def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
   
    encrypt, decrypt, text_bin, master_key =  [], "", [], ""
    
    kadr = int(input("Введите номер кадра: "))
    
    for i in text:
        i = str(bin(ord(i))[2:])
        text_bin.append(i)    
    
    count = 0  
    bits = []  
    temp = ""
    
    while len(bits) < len(text_bin)*11:
            bits += crypt(kadr)
            kadr += 1
            
    for j in range(len(text_bin)):
        test = text_bin[j]
        for t in range(len(test)):
            temp += str(int(bits[count]) ^ int(test[t]))
            count += 1
        temp = int(temp, 2)
        encrypt.append(temp)
        temp = ""
        
    print("\nЗашифрованный текст: " )
    print(encrypt)
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("\nВведите зашифрованный текст: ")

    # Используемый ключ
    kadr = int(input("Введите номер кадра: "))
    
    encrypt, decrypt = text, ""
    encrypt = re.findall('(\d+)',encrypt)
    text_bin = []
    for i in encrypt:
        i = str(bin(int(i))[2:])
        while len(i) != 11:
            i = "0" + i
        text_bin.append(i)  
            
    count = 0  
    bits = []  
    temp = ""
    
    while len(bits) < len(text_bin)*11:
            bits += crypt(kadr)
            kadr += 1
            
    for j in range(len(text_bin)):
        test = text_bin[j]
        for t in range(len(test)):
            temp += str(int(bits[count]) ^ int(test[t]))
            count += 1
        temp = int(temp, 2)
        decrypt += chr(temp)
        temp = ""

    print("Расшифрованный текст: " + decrypt)

def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_6_2.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nА5-2")
      print("1. Зашифровать")
      print("2. Расшифровать")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()