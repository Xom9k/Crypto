# -*- coding: utf-8 -*-

# p = 11
# q = 5
# e = 17

def encryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')
 
    p = int(input("Введите параметр P: "))
    q = int(input("Введите параметр Q: "))
    e = int(input("Введите параметр E: "))
    
    
    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
    
       
    Flag = True
    
    if p > q:
        temp = p
    else:
        temp = q
    
    for i in range (2, temp):
        if p % i == 0 and q % i == 0:
            print("Числа имееют общ делители, Нужны другие")
            Flag = False
    
    if Flag == True:
        n = p * q
        f = (p - 1) * (q - 1)
        
        if e > f:
            temp = e
        else:
            temp = f
        
        for i in range (2, temp):
            if e % i == 0 and f % i ==0:
                print("Числа имееют общ делители, Нужны другие")
                Flag = False
    
        if Flag == True:
            for i in range(100000000000):
                if (i*e) % f == 1:
                    d=i
                    break
            
            S = (x ** d) % n 
        
    print("Используемыe ключи: E = %d; N = %d; D = %d " % (e,n,d))
    print('Получателю отправляется: m=%s и S=%d' % (text,S))
  
def decryption():
    # Используемый алфавит
    alph = "АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ"
    
    # Используемый открытый текст
    text = input("Введите исходный текст: ").upper()
    text = text.replace('Ё', 'Е').replace("-", "ТИРЕ").replace(",", "ЗПТ").replace(".", "ТЧК").replace(' ', '')

    def new_hash(x, i):
        x = ((x + i) ** 2) % 11 
        return x
    
    x = 0
    for i in text:
        index = alph.index(i) 
        x = new_hash(x,index)
 
    n = int(input("Введите параметр N: "))
    e = int(input("Введите параметр E: "))
    S = int(input("Введите полученное S: "))

    rez = (S**e) % n
                
    print('Проверка подписи: m=%d и m`=%d' % (x,rez))


def menu():
    print("\n1. Запустить снова")
    print("2. Перейти в меню")

    temp = input("Сделайте выбор: ")

    if temp == "1":
        exec(open('CRYPTUS_9.py', encoding="utf8").read())
    elif temp == "2":
        exec(open('proges.py', encoding="utf8").read())
        
if __name__ == '__main__':
    try:
      print("\nЭЦП RSA")
      print("1. Вычислить")
      print("2. Проверить")
      
      temp = input("\nСделайте выбор: ")
      
      if temp == "1":
          encryption()
          menu()
      elif temp == "2":
          decryption()   
          menu()
    except:
        print("\n*******************")
        print("Что-то пошло не так")
        print("*******************")
        menu()

